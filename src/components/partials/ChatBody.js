import React, { useContext } from 'react'
import { GlobalContext } from '../../Store/Globalstate';

function ChatBody({ messages, chatWindowRef }) {
    const { state, dispatch } = useContext(GlobalContext);
    // const rcvID = state.receiverID;
    // const sendID = localStorage.getItem('senderID');
    // const msg = messages;
    // alert(sendID);
    // const mess = msg.filter(m => m.senderID= sendID && m.receiverID ===state.receiverID);
    // console.log(mess);
    return (
        <div className="row message chat-window" id="conversation" ref={chatWindowRef}>
            <div className="row message-previous">
                <div className="col-sm-12 previous">
                    <a onclick="previous(this)" id="ankitjain28" name={20}>
                        Show Previous Message!
                    </a>
                </div>
            </div>
            {messages.map(message => (
                message.name === localStorage.getItem("username") ? (
                    <div className="row message-body" key={message.id}>
                        <div className="col-sm-12 message-main-sender">
                            <div className="sender">
                                <div className="message-text">
                                    {message.text}
                                </div>
                                <span className="message-time pull-right">
                                    {message.time}
                                </span>
                            </div>
                        </div>
                    </div>
                ) : (
                    <div className="row message-body" key={message.id}>
                        <div className="col-sm-12 message-main-receiver">
                            <div className="receiver">
                                <div className="message-text">
                                    {message.text}
                                </div>
                                <span className="message-time pull-right">
                                    {message.time}
                                </span>
                            </div>
                        </div>
                    </div>
                )
            ))}
        </div>
    )
}
export default ChatBody
